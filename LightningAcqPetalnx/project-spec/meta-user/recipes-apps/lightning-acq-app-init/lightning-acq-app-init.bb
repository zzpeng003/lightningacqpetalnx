#
# This file is the lightning-acq-app-init recipe.
#

SUMMARY = "Simple lightning-acq-app-init application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://lightning-acq-app-init \
	   file://identify_uio.sh \
		  "

S = "${WORKDIR}"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-rc.d

INITSCRIPT_NAME = "lightning-acq-app-init"
INITSCRIPT_PARAMS = "start 99 S ."

do_install() {
	     install -d ${D}/${bindir}
	     install -d ${D}/${sysconfdir}/init.d
	     install -m 0775 ${S}/identify_uio.sh ${D}/${bindir}/identify_uio
	     install -m 0755 ${S}/lightning-acq-app-init ${D}/${sysconfdir}/init.d/lightning-acq-app-init
}

FILES_${PN} += "${sysconfdir}/*"
