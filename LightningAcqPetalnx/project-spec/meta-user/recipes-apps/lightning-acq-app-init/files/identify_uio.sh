#!/bin/sh

#
# Copyright (C) 2021  West Straits Communication Engineering Center(WSCEC).
# Contact: http://www.wscec.com/
#  

# Use the UIO name to represent the dataprocessing device.
uios=`grep -r "dataProcessing" /sys/class/uio/uio*/name | awk -F '[/:]' '{print "/dev/"$5":"$7}'`
for uio in $uios; do dev=`echo $uio | awk -F ':' '{print$1}'`; name=`echo $uio | awk -F ':' '{print$2}'`; ln -s "$dev" "/dev/$name"; done

# Use the UIO name to represent the dataprocessing interrupts
uios=`grep -r "uio_intr_*" /sys/class/uio/uio*/name | awk -F '[/:]' '{print "/dev/"$5":"$7}'`
for uio in $uios; do dev=`echo $uio | awk -F ':' '{print$1}'`; name=`echo $uio | awk -F ':' '{print$2}'`; ln -s "$dev" "/dev/$name"; done

