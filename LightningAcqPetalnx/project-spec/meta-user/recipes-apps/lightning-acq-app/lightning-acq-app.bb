#
# This file is the lightning-acq-app recipe.
#

SUMMARY = "Simple lightning-acq-app application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://LightningAcqApp \
	   file://fir0Chn0.coef \
	   file://fir1Chn0.coef \
	   file://fir0Chn1.coef \
	   file://fir1Chn1.coef \
	"

S = "${WORKDIR}"

DEPENDS += "mosquitto qtbase qtmqtt"

do_install() {
	     install -d ${D}/${bindir}
	     install -d ${D}/${sysconfdir}
	     install -m 0755 ${S}/LightningAcqApp ${D}/${bindir}
	     install -m 0775 ${S}/fir0Chn0.coef ${D}/${sysconfdir}/fir0Chn0.coef
	     install -m 0775 ${S}/fir1Chn0.coef ${D}/${sysconfdir}/fir1Chn0.coef
	     install -m 0775 ${S}/fir0Chn1.coef ${D}/${sysconfdir}/fir0Chn1.coef
	     install -m 0775 ${S}/fir1Chn1.coef ${D}/${sysconfdir}/fir1Chn1.coef
}

