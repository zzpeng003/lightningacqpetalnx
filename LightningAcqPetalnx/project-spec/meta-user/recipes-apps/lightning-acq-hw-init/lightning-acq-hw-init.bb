#
# This file is the lightning-acq-hw-init recipe.
#

SUMMARY = "Simple lightning-acq-hw-init application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://lightning-acq-hw-init \
	   file://lmk03318_config.sh \
	   file://ad926x_config.sh \
	"

S = "${WORKDIR}"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-rc.d

INITSCRIPT_NAME = "lightning-acq-hw-init"
INITSCRIPT_PARAMS = "start 98 S ."

do_install() {
	     install -d ${D}/${bindir}
	     install -d ${D}/${sysconfdir}/init.d
	     install -m 0755 ${S}/lmk03318_config.sh ${D}/${bindir}/lmk03318_config
	     install -m 0755 ${S}/ad926x_config.sh ${D}/${bindir}/ad926x_config
	     install -m 0755 ${S}/lightning-acq-hw-init ${D}/${sysconfdir}/init.d/lightning-acq-hw-init
}
FILES_${PN} += "${sysconfdir}/*"
