#!/bin/sh

#
# Copyright (C) 2021  West Straits Communication Engineering Center(WSCEC).
# Contact: http://www.wscec.com/
#  

GPIO_CHIP=0

# LMK I2C
LMK_I2C_BUSNUM=0
LMK_I2C_SLVAVER_ADDRESS=0x53

# LMK IOs
LMK_PDN=78
LMK_HW_SW_CTRL=79
LMK_REFSEL=80
LMK_GPIO0=81
LMK_GPIO1=82
LMK_GPIO2=83
LMK_GPIO3=84
LMK_GPIO4=85
LMK_GPIO5=86

# I2C Operate
i2c_write()
{
	busnum=$1
	slaverAddress=$2
	dataAddress=$3
	value=$4
	
	i2cset -f -y $busnum $slaverAddress $dataAddress $value
}

i2c_write_then_read()
{
	i2c_write $1 $2 $3 $4
	
	retval=`i2cget -f -y $1 $2 $i3 | tr [a-z] [A-Z]`
	value=`echo $4 | tr [a-z] [A-Z]`
	
	if [ ! $retval == $value ]; then
		printf "address=$dataAddress retval($retval) != value($value), please verify the description of this register in the data sheet.\r\n"
	fi
}

lmk_i2c_write()
{
	i2c_write $LMK_I2C_BUSNUM $LMK_I2C_SLVAVER_ADDRESS $1 $2
}

lmk_i2c_write_then_read()
{
	i2c_write_then_read $LMK_I2C_BUSNUM $LMK_I2C_SLVAVER_ADDRESS $1 $2
}

##################
# LMK GPIO INIT
##################

# config refsel
gpioset $GPIO_CHIP $LMK_REFSEL=1

# config hw_sw_ctrl: select soft-pin mode
gpioset $GPIO_CHIP $LMK_HW_SW_CTRL=0

# config GPIO1: i2c slaver address
if [ $LMK_I2C_SLVAVER_ADDRESS == 0x53 ]; then
	gpioset $GPIO_CHIP $LMK_GPIO1=1
else
	gpioset $GPIO_CHIP $LMK_GPIO1=0
fi

# config GPIO[0]
gpioset $GPIO_CHIP $LMK_GPIO0=1

# config GPIO[3:2]
gpioset $GPIO_CHIP $LMK_GPIO3=0
gpioset $GPIO_CHIP $LMK_GPIO2=0

# lmk toggle pdn: power off -> power on
gpioset $GPIO_CHIP $LMK_PDN=0
gpioset $GPIO_CHIP $LMK_PDN=1

##################
# start write SRAM
##################

# 1. Program the device registers to match a desired setting.
lmk_i2c_write_then_read 0x00 0x10
lmk_i2c_write_then_read 0x01 0x0B
lmk_i2c_write_then_read 0x02 0x33
lmk_i2c_write_then_read 0x03 0x02
lmk_i2c_write_then_read 0x04 0x00
lmk_i2c_write_then_read 0x05 0x00
lmk_i2c_write_then_read 0x06 0x00
lmk_i2c_write_then_read 0x07 0x00
lmk_i2c_write_then_read 0x08 0x00
lmk_i2c_write_then_read 0x09 0x00
lmk_i2c_write_then_read 0x0A 0xA0
lmk_i2c_write_then_read 0x0B 0x00
lmk_i2c_write_then_read 0x0C 0xDF
lmk_i2c_write_then_read 0x0D 0x00
lmk_i2c_write_then_read 0x0E 0x1D
lmk_i2c_write_then_read 0x0F 0x00
lmk_i2c_write_then_read 0x10 0x00
lmk_i2c_write_then_read 0x11 0x00
lmk_i2c_write_then_read 0x12 0x00
lmk_i2c_write_then_read 0x13 0x00
lmk_i2c_write_then_read 0x14 0xFF
lmk_i2c_write_then_read 0x15 0xFF
lmk_i2c_write_then_read 0x16 0xFF
lmk_i2c_write_then_read 0x17 0x03
lmk_i2c_write_then_read 0x18 0x00
lmk_i2c_write_then_read 0x19 0xF5
lmk_i2c_write_then_read 0x1A 0x00
lmk_i2c_write_then_read 0x1B 0x28
lmk_i2c_write_then_read 0x1C 0x18
lmk_i2c_write_then_read 0x1D 0x02
lmk_i2c_write_then_read 0x1E 0x00
lmk_i2c_write_then_read 0x1F 0x30
lmk_i2c_write_then_read 0x20 0x30
lmk_i2c_write_then_read 0x21 0x3B
lmk_i2c_write_then_read 0x22 0x30
lmk_i2c_write_then_read 0x23 0x30
lmk_i2c_write_then_read 0x24 0x3B
lmk_i2c_write_then_read 0x25 0x18
lmk_i2c_write_then_read 0x26 0x3B
lmk_i2c_write_then_read 0x27 0x18
lmk_i2c_write_then_read 0x28 0x3B
lmk_i2c_write_then_read 0x29 0x18
lmk_i2c_write_then_read 0x2A 0x3B
lmk_i2c_write_then_read 0x2B 0x18
lmk_i2c_write_then_read 0x2C 0x3B
lmk_i2c_write_then_read 0x2D 0x0A
lmk_i2c_write_then_read 0x2E 0x00
lmk_i2c_write_then_read 0x2F 0x00
lmk_i2c_write_then_read 0x31 0x00
lmk_i2c_write_then_read 0x32 0x07
lmk_i2c_write_then_read 0x33 0x03
lmk_i2c_write_then_read 0x34 0x00
lmk_i2c_write_then_read 0x35 0x00
lmk_i2c_write_then_read 0x36 0x00
lmk_i2c_write_then_read 0x37 0x00
lmk_i2c_write_then_read 0x38 0x06
lmk_i2c_write_then_read 0x39 0x18
lmk_i2c_write_then_read 0x3A 0x00
lmk_i2c_write_then_read 0x3B 0x3C
lmk_i2c_write_then_read 0x3C 0x00
lmk_i2c_write_then_read 0x3D 0x00
lmk_i2c_write_then_read 0x3E 0x00
lmk_i2c_write_then_read 0x3F 0x00
lmk_i2c_write_then_read 0x40 0x00
lmk_i2c_write_then_read 0x41 0x01
lmk_i2c_write_then_read 0x42 0x0C
lmk_i2c_write_then_read 0x43 0x04
lmk_i2c_write_then_read 0x44 0x01
lmk_i2c_write_then_read 0x45 0x04
lmk_i2c_write_then_read 0x46 0x07
lmk_i2c_write_then_read 0x47 0x1F
lmk_i2c_write_then_read 0x48 0x18
lmk_i2c_write_then_read 0x49 0x00
lmk_i2c_write_then_read 0x4A 0x32
lmk_i2c_write_then_read 0x4B 0x00
lmk_i2c_write_then_read 0x4C 0x00
lmk_i2c_write_then_read 0x4D 0x00
lmk_i2c_write_then_read 0x4E 0x00
lmk_i2c_write_then_read 0x4F 0x00
lmk_i2c_write_then_read 0x50 0x01
lmk_i2c_write_then_read 0x51 0x0C
lmk_i2c_write_then_read 0x52 0x10
lmk_i2c_write_then_read 0x53 0x01
lmk_i2c_write_then_read 0x54 0x04
lmk_i2c_write_then_read 0x55 0x07
lmk_i2c_write_then_read 0x56 0x00
lmk_i2c_write_then_read 0x57 0x00
lmk_i2c_write_then_read 0x58 0x00
lmk_i2c_write_then_read 0x59 0xDE
lmk_i2c_write_then_read 0x5A 0x01
lmk_i2c_write_then_read 0x5B 0x18
lmk_i2c_write_then_read 0x5C 0x01
lmk_i2c_write_then_read 0x5D 0x4B
lmk_i2c_write_then_read 0x5E 0x01
lmk_i2c_write_then_read 0x5F 0x86
lmk_i2c_write_then_read 0x60 0x01
lmk_i2c_write_then_read 0x61 0xBE
lmk_i2c_write_then_read 0x62 0x01
lmk_i2c_write_then_read 0x63 0xFE
lmk_i2c_write_then_read 0x64 0x02
lmk_i2c_write_then_read 0x65 0x47
lmk_i2c_write_then_read 0x66 0x02
lmk_i2c_write_then_read 0x67 0x9E
lmk_i2c_write_then_read 0x68 0x00
lmk_i2c_write_then_read 0x69 0x00
lmk_i2c_write_then_read 0x6A 0x05
lmk_i2c_write_then_read 0x6B 0x0F
lmk_i2c_write_then_read 0x6C 0x0F
lmk_i2c_write_then_read 0x6D 0x0F
lmk_i2c_write_then_read 0x6E 0x0F
lmk_i2c_write_then_read 0x73 0x08
lmk_i2c_write_then_read 0x74 0x19
lmk_i2c_write_then_read 0x75 0x00
lmk_i2c_write_then_read 0x76 0x07
lmk_i2c_write_then_read 0x77 0x05
lmk_i2c_write_then_read 0x78 0x00
lmk_i2c_write_then_read 0x79 0x0F
lmk_i2c_write_then_read 0x7A 0x0F
lmk_i2c_write_then_read 0x7B 0x0F
lmk_i2c_write_then_read 0x7C 0x0F
lmk_i2c_write_then_read 0x81 0x08
lmk_i2c_write_then_read 0x82 0x19
lmk_i2c_write_then_read 0x83 0x00
lmk_i2c_write_then_read 0x84 0x03
lmk_i2c_write_then_read 0x85 0x01
lmk_i2c_write_then_read 0x86 0x00
lmk_i2c_write_then_read 0x87 0x00
lmk_i2c_write_then_read 0x88 0x00
lmk_i2c_write_then_read 0x89 0x10
lmk_i2c_write_then_read 0x8A 0x00
lmk_i2c_write_then_read 0x8B 0x00
lmk_i2c_write_then_read 0x8C 0x00
lmk_i2c_write_then_read 0x8D 0x00
lmk_i2c_write_then_read 0x8E 0x00
lmk_i2c_write_then_read 0x8F 0x00
lmk_i2c_write_then_read 0x90 0x00
lmk_i2c_write_then_read 0x91 0x00
lmk_i2c_write_then_read 0xA9 0x40
lmk_i2c_write_then_read 0xAC 0x24
lmk_i2c_write_then_read 0xAD 0x00

# 2. Write R145[3:0] with a valid SRAM page (0 to 5) to commit the current register data.
lmk_i2c_write_then_read 145 0x00

# 3. Write a 1 to R137.6. This ensures that the device registers are copied to the desired SRAM page.
lmk_i2c_write 137 0xC0


