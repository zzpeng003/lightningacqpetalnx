#!/bin/sh

#
# Copyright (C) 2021  West Straits Communication Engineering Center(WSCEC).
# Contact: http://www.wscec.com/
#

GPIO_CHIP=0

# AD926X SPI
AD926X_SPI_DEV=/dev/spidev1.0
AD926X_SPI_CPOL=1
AD926X_SPI_CPHA=1

# AD926X IOs
AD926X_PDWN=87
AD926X_OEB=88
AD926X_CSB=89
AD9269_SYNC=90

# SPI Operate
spi_write()
{
    device=$1
    cpol=$2
    cpha=$3
    data=$4
    spidev_test -D $device --cpol $cpol --cpha $cpha -p $data -C
}

ad926x_spi_write()
{
    spi_write $AD926X_SPI_DEV $AD926X_SPI_CPOL $AD926X_SPI_CPHA $1
}


###################
# AD926X GPIO INIT
###################

# power on
gpioset $GPIO_CHIP $AD926X_PDWN=0

# chip select
gpioset $GPIO_CHIP $AD926X_CSB=0

# output disable
gpioset $GPIO_CHIP $AD926X_OEB=1

###################
# Start SPI Write
###################
ad926x_spi_write "\x00\x00\x18" # SPI port configuration
ad926x_spi_write "\x00\xff\xff" 
ad926x_spi_write "\x00\x05\x03" # Channel index
ad926x_spi_write "\x00\xff\xff"
ad926x_spi_write "\x00\x0b\x00" # Clock divider
ad926x_spi_write "\x00\xff\xff"
ad926x_spi_write "\x00\x14\x81" # Output mode
ad926x_spi_write "\x00\xff\xff"
ad926x_spi_write "\x00\x15\x22" # OUTPUT_ADJUST
ad926x_spi_write "\x00\xff\xff"
ad926x_spi_write "\x00\x17\x07" # OUTPUT_DELAY

# output enable
gpioset $GPIO_CHIP $AD926X_OEB=0
