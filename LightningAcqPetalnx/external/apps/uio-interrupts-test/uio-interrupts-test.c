#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


int main(int argc, char *argv[])
{
	int fd;
	char *dev_path;
	
	if (argc != 2) {
		printf("Usage: %s /dev/uioX\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	dev_path = argv[1];

	if ((fd = open(dev_path, O_RDWR)) < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	while (1) { /* some condition here */
		uint32_t info = 1; /* unmask */

		ssize_t nb = write(fd, &info, sizeof(info));
		if (nb != (ssize_t)sizeof(info)) {
			perror("write");
			close(fd);
			exit(EXIT_FAILURE);
		}

		/* Wait for interrupt */
		nb = read(fd, &info, sizeof(info));
		if (nb == (ssize_t)sizeof(info)) {
			/* Do something in response to the interrupt. */
			printf("Interrupt #%u!\n", info);
		}
	}

	close(fd);
	exit(EXIT_SUCCESS);
}
